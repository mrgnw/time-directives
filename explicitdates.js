angular
    .module('timelord', [])
    .directive('datetime', datetime)
    .directive('date', date)
    .directive('time', time)
    .directive('duration', duration)
    .directive('dtinput', dtinput)
    .controller('timeController', timeController);

// just for our test html page
function timeController($scope) {
    var vm = this;
    vm.bday = "1988-02-12";    
    vm.lunch = "2010-10-20 12:15 PM";
    vm.anniv = "Apr 30, 2011";
}

// foundation for all text views
// all other dt directives are just templates that use this
function datetime() {
    return {
        restrict: 'E',
        template: '{{saveAs}}',
        scope: {
            source: '=',
            format: '@',
            defaultFormat: '@'
        },
        // controllerAs: 'ctrl',
        // controller: function() {
        //     var vm = this;
        // },
        link: function (scope, element, attrs, $timeout) {
            // backupFormat: in case we don't define a defaultFormat
            // in the other directive templates

            // todo: optimize
            scope.$watch(attrs.source, function(newValue) {

                var backupFormat = 'YYYY-MM-DD';
                var format = scope.format || scope.defaultFormat ||backupFormat;
                // todo: add formats[]
                var m = moment(scope.source);
                
                var newNewValue = m.format(format, "YYYY-MM-DD");
                element.text(newNewValue);
            });
        }
    }
}


function dtinput() {
    return {
        restrict: 'E',
        template: '<input type="text"' +
                    'ng-model="bob" ' +
                    'placeholder="{{placeholder}}"' + 
                    '">',
        scope: {
            source: '=',
            format: '@',
            placeholder: '@',
            value: '='
        },
        link: function (scope, element, attrs, $timeout) {
            var defaultFormat = "YYYY-MM-DD HH:MM";
            var format = scope.format || defaultFormat;

            // If you don't set value to source, value will be Date();
            initInput(scope);
            // focusVal tracks changes
            var focusVal = scope.bob;
            // $scope.focused(initInput(scope));
            
            // set & get value of input
            element.val(scope.bob);
            // console.log("WHOA", element.val());

            // watch ng-model (currently scope.bob)
            scope.$watch('bob', function() {
                var m = moment(scope.bob);
                var newVal = m.format(format);

                if ( m.isValid() || ((newVal) === "")) {
                    // AND if this field isn't in focus
                    scope.source = newVal;
                    // todo: input = clean
                }
                else {
                    // todo: input = dirty
                }
            });

            // todo: only update when you're not typing
            scope.$watch('source', function() {
                //todo: if not focused
                if (scope.bob != scope.source){
                    // initInput(scope);
                }
                
            });

            // for reusability - onFocus, etc.
            function initInput(scope){
                scope.bob = scope.source;
            }

            function isBobEmpty(scope) {
                return (scope.bob === "" || scope.bob == undefined );
            }
        }
    }
}


function date() {
    return {
        restrict: 'E',
        template: '<datetime source="source" ' +
                  'defaultFormat="YYYY-MM-DD H" ' + 

                  // this dude. This dude right here
                  'format="{{format}}" ></datetime>',
        scope: { source: '=', format: '@', defaultFormat: '@' },
    }
}

function time() {
    return {
        restrict: 'E',
        template: '<datetime source="source" ' +
                  'defaultFormat="H:mm" ' + 
                  'format="{{format}}" ></datetime>',
        scope: { source: '=', format: '@', defaultFormat: '@' },
    }
}

function duration() {
    return {
        restrict: 'E',
        template: '<datetime source="source" ' +
                  'defaultFormat="HH:mm" ' + 
                  'format="{{format}}" ></datetime>',
        scope: { source: '=', format: '@', defaultFormat: '@' },
    }
}