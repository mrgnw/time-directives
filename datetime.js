angular
    .module('timelord', [])
    .directive('datetime', datetime) // + <dt>  *************************
    // .directive('date', date) // + <birthdate>?
    .controller('timeController', timeController);


function datetime() {
    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        template: '<span ng-transclude>{{saveAs}}</span>',
        scope: {
            source: '@',
            saveAs: '=', // what is displayed
            format: '=',
            defaultFormat: '='
        },
        controllerAs: 'ctrl',
        controller: function() {
            var vm = this;
            vm.format = vm.format || vm.defaultFormat;
        },

        link: function (scope, element, attrs) {
            // in each directive, format = format || defaultFormat;
            // scope.defaultFormat = 'YYYY-MM-DD @ H:MM:SS';

            var m = moment(scope.source); // todo: add formats[]
            var output = m.format(scope.defaultFormat);
            console.log(scope.source); // binding werks here
            console.log(output);

            // todo: set saveAs correctly
            // scope.saveAs = output;

        }
    }
}

// A copy of datetime with a different defaultFormat
// function date() {
//     return {
//         restrict: 'E',
//         transclude: true,
//         replace: true,
//         scope: {
//             source: '@', saveAs: '?=', format: '?=',
//             // This is the ONLY difference from datetime
//             defaultFormat: 'D MMM YYYY'
//         },
//         template: '<datetime ng-transclude></ng-transclude>',
//     }
// }


function timeController($scope) {
    var vm = this;
    vm.bob = "02-12-1988";
    vm.jane = "yay!";

}