// switching to ngModel binding

angular
    .module('directives.birthdatePicker.js', [])
    .directive('bdayPicker', bdayPicker)
    .controller('bdayPickerController', bdayPickerController);

function bdayPicker () {
    return {
        restrict: 'E',
        scope: {
            minAge: '=?',
            maxAge: '=?',
            formatIn: '=?', // Accept a list?
            formatOut: '=?', //"yyyy-mm-dd"
            // locale: set formatIn by locale?
            input: '=',
            result: '='
        },

        templateUrl: 'common/directives/birthdatePicker/' +
            'birthdatePicker.tpl.html',
        controller: 'bdayPickerController',
        controllerAs: 'bdayCtrl',
        bindToController: true,

        // Allow ngModel binding
//        require: "ngModel",
        link: linkFunk
    };

    function linkFunk (scope, element, attrs, ngModel) {
            element.on("click", function () {

                // sample code
                ngModel.$setViewValue(ngModel.$viewValue + 10);
                //ngModel.$formatters
                //ngModel.$parsers
                //ngModel.$validators
                scope.apply();
            });
        }
}


function bdayPickerController ($scope, $filter, moment) {
    var vm = this;
    vm.input = input;

    vm.valiDate = valiDate;
    vm.formatDate = formatDate;

    vm.activate = activate;

    // on change validate & update result

    function activate() {
        // initialize variables
        vm.minAge = $scope.minAge || 12;
        vm.maxAge = $scope.maxAge || 180;
//        var formatIn = $scope.formatIn || 12;
        vm.formatOut = $scope.formatOut || 'YYYY-MM-DD';
        vm.input = $scope.input; // is this necessary? Or can we just do the element
        vm.result = $scope.result;

    }

    // create moment
    var m = moment("2011-10-10T10:20:90");


    // BONUS VALIDATION
    // adding / subtracting time from today
    //    var minAgeDate = moment().subtract(12, 'years');
    //    var maxAgeDate = moment().subtract(150, 'years');

    // make sure m fits in this range
    //    var actual = m.min(minAgeDate).max(maxAgeDate); // double check
    //    var inRange = actual === m;


    function valiDate() {
        return m.isValid();
    }

    function formatDate() {
        vm.result = m.format(vm.formatOut);
    }






    vm.input = ''; // when source changes...
    vm.result = ''; // ... update result (with validated & formatted version)

    // todo: initialize input with result

    vm.opened = false; // popup is not open until you click and activate openPopup()
    vm.open = openPopup;

    // set default values for formatIn, formatOut

    vm.getMinDate = getMinDate;
    vm.getMaxDate = getMaxDate;




    $scope.$watch('bdayCtrl.source', function ($format, formatOut, source, result) {
        // format input.
        vm.result = $filter('date')(Date.now(), vm.formatOut);


//        if(oldUser && !_.isEqual(newUser, oldUser)) {
//                _.forOwn(newUser, _updateField, oldUser);
//            }
//        }, true);
    });


    function openPopup ($event) {
        $event.preventDefault();
        $event.stopPropagation();

        vm.opened = true;

    }

    // use minAge & maxAge to getMinDate and getMaxDate
    function getMinDate () {
        // set default values for minAge, maxAge
        // if non-existent, vm.maxAge = 150;
        var today = new Date();
        return today.dateAdd('year', -1 * vm.maxAge);
    }

    function getMaxDate () {
        // set default values for minAge, maxAge
        // if non-existent, vm.minAge = 12;
        var today = new Date();
        return today.dateAdd('year', -1 * vm.minAge);
    }




}