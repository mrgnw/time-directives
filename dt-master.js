// q: reusability: use the same link function, controller, or simply transclude template?
angular
    .module('directives.xdatetime.js', []) // todo: rename xdatetime to something safe
    
    // view as text
    .directive('datetime', datetime) // + <dt>  *************************
    .directive('date', date) // + <birthdate>?
    .directive('time', time)
    .directive('duration', duration) // + <dur>

    // edit as text
    .directive('datetimeInput', datetimeInput) // + <dt-input>   ********
    .directive('dateInput', dateInput)
    .directive('timeInput', timeInput)
    .directive('durationInput', durationInput) // + <dur-input>

    // view or edit with custom view
    .directive('datetimePicker', datetimePicker) // *********************
    .directive('calendar', calendar)
    .directive('clock', clock)
    .directive('durationView', durationView)
    // ^ polar, circle, circles, bar, bars, (shard?) type= or <dur-bar> (A or E)
    // Also, taco view: How many tacos could you buy pased on your payrate * time?
    
    // .controller('dtTextController', dtTextController);
    // text controller?
    // input controller?
    // custom controllers: clock, calendar, durationView


function datetime() {
    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        template: '<span ng-transclude></span>',
        scope: {
            source: '@',
            saveAs: '?=', // what is displayed
            format: '?=',
            // in each directive, format = format || defaultFormat;
            defaultFormat: 'YYYY-MM-DD @ H:MM:SS'
        },
        link: datetimeTextLink
    }
    
    function datetimeTextLink (scope, element, attrs) {
        var m = moment(scope.source);

        scope.$apply(function() {
            // Q: does this work for setting default value?
            scope.saveAs = moment.format(format || defaultFormat);
        });
    }
}

// A copy of datetime with a different defaultFormat
function date() {
    return {
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: {
            source: '@', saveAs: '?=', format: '?=',
            // This is the ONLY difference from datetime
            defaultFormat: 'D MMM YYYY'
        }
        template: '<datetime ng-transclude></datetime>',
    }
}

// v2: no transclude
function date() {
    return {
        restrict: 'E',
        template: '<datetime source="source">'
    }

}

function datetimeInput() {
    return {
        restrict: 'E',
        scope: {
            source: '=',
            saveAs: '=',
            // input format is automatically assumed
            format: '?=', // this is the output sent to saveAs object
            minDate: '?=',
            maxDate: '?=',
            // openOnFocus?
        },
        templateUrl: 'common/directives/xdatetimeInput/' + 'xdatetimeInput.tpl.html',
        controller: 'datetimeInputController',
        controllerAs: 'datetimeInputCtrl',
        bindToController: true
    }
}





// EXAMPLE --> Date basically inherits datetime, with defaults associated with date.
function date() {
    return {
        restrict: 'E',
        scope: false,
        template: '<datetime format="date"/>'
    }
}


function dtTextController ($scope, $filter, moment) {
    var vm = this;

    var m = moment(dt, format);

}


function dtInputController ($scope, $filter, moment) { }
function clockController ($scope, $filter, moment) { }
function calendarController ($scope, $filter, moment) { }
function durationViewController ($scope, $filter, moment) { }