# The plan:
Create a set of datetime directives. The primary benefits are 1) Safely bind to the model, thanks to built-in validation & format handling, so you can  2) significantly cleaner HTML

View & edit times, dates, datetimes, and durations with simple HTML tags and format guides.
i.e. <date save-as="ctrl.birthdate" formatIn="YYYY-MM-DD" formatOut="YYYY-MM-DD">
The models handel formatting, parsing, and all the dirty work so you can just update the model easily, while keeping it backend-compatible & pretty enough for the frontend without loads of custom code.

# Overview 
## Tags
### View as text
- `<datetime>` or `<dt>`
- `<date>`
- `<time>`
- `<duration>` or `<dur>`

### Edit as text
- `<datetime-input>` or `<dt-input>`
- `<date-input>`
- `<time-input>`
- `<duration-input>` or `<dur-input>`

### View or edit with custom visual form
- `<datetime-picker>` or `<dt-picker>`
- `<calendar>`
- `<clock>`
- `<timer>`? or `<duration-picker>` or `<dur-picker>` or `<polar-clock>`


Popup:
`<calendar-popup>` or `<calendar type="popup">`

## Usage
- Text:  `<date source="birthdate" format="D M Y"/>`
- Input: `<date save-as="birthdate" format="YYYY-MM-DD"/>` Automatically determines input format.
- Custom: `<cal save-as="birthdate" format="D M YYYY"/>`
  - ^ If source is defined, it's view-only. If save-as, it's editable.




## Format examples
Set inputFormat and outputFormat using anything compatible with [moment.js](http://momentjs.com)

### Time
11:30 pm || 23:30 || eleven-thirty p.m. || pretty late at night

### Duration: 
5h 30m || 5:30 || 5 hours 30 minutes

### Date
2/12/1988 || 5 May 2012 || etc

### Datetime
Jan 23 @ 5pm

### [Timeless](https://github.com/stormpat/ng-timeless)
45 minutes ago

use directive [`<timeless>`](https://github.com/stormpat/ng-timeless)


## Notes
Really any of these could be done with the datetime, but having separate *time* and *duration* will provide useful defaults and make HTML much easier to read.

+ Open-source?
+ use momentjs to keep our code simple.
